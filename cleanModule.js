var path = require('path'),
    fs = require('fs'),    
    filewalker = require('filewalker');
  var DOMParser = require('xmldom').DOMParser;


/*-------------------------------------------------------------*\
 * RegEx Replacements
\*-------------------------------------------------------------*/
var replacements = [
  {
    // Hide PlayBar
    regex: /hasPlayBar\:\s?true/g,
    count: 1,
    replace: function (match) {
      return 'hasPlayBar:false';
    }
  },
  { // Remove the funky grey background/border from where the playbar is.
    regex: /hasBorder\:\s?true/g,
    count: 1,
    replace: function (match) {
      return 'hasBorder:false';
    }
  },
  {
    // Recalculate the height to exclude the footer. See a.SetScaleAndPosition in CPM.js
    regex: /b\s?=\s?b.innerHeight;/g,
    count: 1,
    replace: function (match) {
      return 'b=b.innerHeight-85;';
    }
  },
  // {
  //   // Make sure the footer is relative positioned.
  //   regex: /text-align:center; z-index: 10000;/g,
  //   count: 1,
  //   replace: function (match) {
  //     return 'text-align:left; z-index: 10000;';
  //   }
  // },
  // {
  //   // Add in final content if it doesn't exist.
  //   regex: /(#content-holder , #menu-right\{\s*border: 0;\s*}\s*})$/g,
  //   count: 1,
  //   replace: function (match) {
  //     return '#content-holder , #menu-right{\n border: 0;\n }\n }\n\n .time-save-and-exit {\n margin-top: 2px;\n }\n\n #playbarWrapper {\n width: 100%;\n height: 41px;\n display: inline-block;\n }\n\n #playbar {\n position: absolute;\n left: 270px;\n top: -97px;\n height: 135px;\n width: calc(100% - 493px);\n cursor: pointer;\n }';
  //   }
  // },
  {
    // Add in final content if it doesn't exist.
    regex: /angular\.min\.js"><\/script>\s*<script src="assets\/js\/app.js">/g,
    count: 1,
    replace: function (match) {
      return 'angular.min.js"></script>\n' +
              '<script src="https://cdnjs.cloudflare.com/ajax/libs/gl-matrix/2.3.1/gl-matrix-min.js"></script>\n' +
              '<script src="assets/js/playbar.min.js"></script>\n' +
              '<script src="assets/js/app.js">';
    }
  },
  {
    // Update footer
    regex: /class="player-controls">[\s\S]*<\/footer>/g,
    count: 1,
    replace: function (match) {
      console.log('Found footer');
      return 'class="player-controls">\n      <div class="push-right time-save-and-exit">\n        <span class="time" ng-cloak>{{time}}</span>\n        <a href="javascript://" class="button" ng-click="saveAndExitHandler()">Save Progress</a>\n      </div>\n\n      <a href="#" class="push-left control {{player_status}}" ng-click="togglePlayPause()"></a>\n      <span class="push-left more white" style="min-width: 120px;">\n        <a href="#" class="push-left control prev-slide" ng-click="playPreviousSlide()"></a>\n        <span ng-cloak>{{current_slide}} of {{total_slides}}</span>\n        <a href="#" class="push-right control next-slide" ng-click="playNextSlide()"></a>\n      </span>\n      <a href="#" class="push-left control {{sound_status}}" ng-click="toggleSound()"></a>\n\n      <div id="playbarWrapper">\n        <canvas id="playbar"></canvas>\n      </div>\n      \n    </footer>';
    }
  },
  {
    find: '<div id="play-overlay-wrapper" class="play-overlay-wrapper-fade" ng-if="player_status==\'play\'">\r\n        <div id="play-overlay">\r\n          <a href="#" class="control {{player_status}}-overlay" ng-click="togglePlayPause()">\r\n            <i class="fa fa-play"></i>\r\n          </a>\r\n        </div>\r\n      </div>',
    replace: function (match) {
      return '<a href="#" class="control {{player_status}}-overlay" ng-click="togglePlayPause()" ng-if="player_status==\'play\'">\n <div id="play-overlay-wrapper" class="play-overlay-wrapper-fade">\n <div id="play-overlay">\n <i class="fa fa-play"></i>\n </div>\n </div>\n </a>';
    }
  },
  {
    regex: /("\/\/cdnjs)/g,
    count: 1,
    replace: function (match) {
      return '"http://cdnjs';
    }
  }
];

/*-------------------------------------------------------------*\
 * Walk the Files
\*-------------------------------------------------------------*/
filewalker(process.argv[2] || './')
  .on('file', function(p, s) {
    // Search only for CPM.js and replace as appropriate.
    if (p.search(/CPM\.js/g) != -1 ||
        p.search(/acumenity\.css/g) != -1 ||
        p.search(/imsmanifest\.xml/g) != -1 ||
        p.search(/index_scorm\.html/g) != -1) {
      processFile((process.argv[2] || './') + p);
    }
  })
  .on('dir', function (p, s) {
    if (p.search(/assets\/js$/g) != -1) {
      fs.createReadStream('template/app.js').pipe(fs.createWriteStream(p + '/app.js'));
      fs.createReadStream('template/playbar.min.js').pipe(fs.createWriteStream(p + '/playbar.min.js'));
    }
    if (p.search(/assets\/css$/g) != -1) {
      fs.createReadStream('template/acumenity.css').pipe(fs.createWriteStream(p + '/acumenity.css'));
    }
  })
  .on('done', function() {
   // console.log('%d dirs, %d files, %d bytes', this.dirs, this.files, this.bytes);
  })
.walk();

/*-------------------------------------------------------------*\
 * Process the file contents and resave it.
\*-------------------------------------------------------------*/
function processFile(filename) {  
  var contents = fs.readFileSync(filename).toString();

  if (filename.indexOf("imsmanifest.xml")!= -1){   
    console.log('begin updating manifest file...') ;
    var activityCode = filename.split('/')[1];
    console.log('activity code is '+ activityCode+"...");
    var xmlDoc = new DOMParser().parseFromString(contents,  "application/xml");    
    var children = xmlDoc.childNodes;     
    var manifest = children[1];

    for (var i = 0; i < manifest.childNodes.length; i++) {
      var child = manifest.childNodes[i];
      if (child != undefined && child != null){
        
        if (child.localName == "resources"){
          var newChild = '<resources><resource identifier="Course_ID1_RES" type="webcontent" href="index_scorm.html" adlcp:scormtype="sco"><file href="index_scorm.html"/></resource></resources>';
          newChildXml = new DOMParser().parseFromString(newChild,  "application/xml");                    
          manifest.replaceChild(newChildXml, child);
        }
      }
    }    
    contents = '<?xml version="1.0" encoding="utf-8" ?>';
    contents += manifest.toString();
    console.log('removed resources node from manifest file...');
    contents = contents.replace(/Course_ID1/g, activityCode);
    contents = contents.replace(/SCO_ID1/g, activityCode);      
    console.log('updating IDs in manifest file...') ;
  }
  else{
    console.log('updating CSS styles and HTML template');
    replacements.forEach(function (r) {
      if (r.find) {
        if (contents.indexOf(r.find) != -1) {
          console.log('Found button');
          contents = contents.replace(r.find, r.replace());
          return;
        }
        else return;
      }

      var matches = contents.match(r.regex);
      if (!matches) return;
      if (matches.length != r.count)
        throw 'ERROR (' + filename + '): Mismatch on expected match count for ' + r.regex + '. Found ' + matches.length;

      matches.forEach(function (m) {
        var replacement = r.replace(m)
        contents = contents.replace(m, replacement);
      });
    });
}
  fs.writeFileSync(filename, contents);
}
