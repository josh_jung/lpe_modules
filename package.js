var fs = require('fs');
var process = require('child_process');
var fw = require('filewalker');
var path = require('path');

fw('./')
  .on('dir', function (p, s) {
      var bn = path.basename(p);
      var z = bn + '.zip';
      if (bn.indexOf('P_') == 0 || bn.indexOf('L_') == 0) {
        // Remove any current zip file
        if (fs.existsSync(z)) {
            console.log('Deleting ' + z);
          fs.unlinkSync(z);
        }
        // Fork process to zip directory contents
        process.exec('zip -r ' + '../' + path.basename(p) + '.zip .', {
            cwd: p
          }, 
          function () {
            console.log('Finished zipping ' + p);
          });
      }
  })
  .on('done', function() {
    console.log('Done triggering zips');
  }).walk();
