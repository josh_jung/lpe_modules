Intuit Pro Series / Lacerte Module deliverables
----

Maintained by [Joshua Jung](mailto:josh@acumenity.com)

Requirements
----

Node.js 0.12.0+

    npm install

Purpose
----

1) Clean modules produced by Adobe Captivate for ProSeries and Lacerte training
2) Package modules in a ZIP file so that they are ready to be uploaded to SumTotal 
   (or any other Scorm compatible online e-learning DB)

Technical Background
----

Adobe Captivate exports a Captivate module in a Scorm compatible file/folder layout
with the required `imsmanifest.xml`.

It includes its own default styling and HTML layout, contained in `index_scorm.html`
and `assets/css`. All the Javascript functionality to drive pause, play, mute, and
render the timeline for playback is contained in the minified `assets/js/CPM.js`.

The primary goal of this utlity is to wrap the default Captivate export in a 
completely different layout, injecting whatever code was necessary so that it would
not break any Captivate functionality.

In order for this to work we had to do the following high-level items:

* Replace the Captivate generated playbar with a completely custom one
* Inject a header above the content
* Resize the content appropriately to make room for the header and playbar
* Hook up all play, pause, playbar click, etc. functionality to the Captivate API

The term API is used loosely in this context. There is no documentation for anything
in the `CPM.js` file. As a result, every API element had to be found by de-uglifying
the JS file and then searching line by line.

Once each piece of functionality was found, we use Regex to find/replace old code 
with the new functionality.

Regex Replacement is driven by `cleanModule.js`. This file searches every directory
within `lpe_modules` for:

* `CPM.js`
* `index_scorm.html`
* `acumenity.css`

`cleanModule.js` then runs through a series of complex regexs to do the replacements.

Note that `acumenity.css` is styling related only to the Intuit styling and is in addition
to the styling provided by the Captivate export.

Cleaning the Modules
----

To clean the modules, place all Captivate Scorm exported module folders (unzipped) directly
in the root of `lpe_modules`.

Then run:

    node cleanModules.js

This will provide some console output for all the changes it is making.

Packaging the Modules
----

In order to upload to a Scorm database, all the files for a module need to be zipped with
the manifest file in the root.

A custom utility was developed to zip every folder that begins with `P_` or `L_` (ProSeries
or Lacerte). These are placed in the root of `lpe_modules`.

Run it with:

    node package.js
