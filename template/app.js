'use strict';

var canvas = document.getElementById('playbar');

window.renderer = new Jungle.GraphicRenderer(canvas, {
  adjustSizeToCSS: true
});

window._playbar = new Playbar({
  total: 100,
  current: 0,
  boundsPercent: new Jungle.util.Rect(0.0, NaN, 1.0, NaN),
  bounds: new Jungle.util.Rect(0, 53, 0, 20),
  chapterRadius: 4,
  playPointRadius: 9,
  playPointWidth: 6,
  playedColor: '#365dbf',
  playPointBorderColor: '#365dbf',
  chapterFillColor: '#365dbf',
  shadow: {
    offsetX: 2,
    offsetY: 2,
    color: 'rgba(0,0,0,0.5)',
    blur: 3
  }
}, 'playbar');

renderer.addChild(_playbar);

angular.module('SFService', []).service('SFService', ['$http', '$q', function($http, $q) {
  var postData = {};
  var courseID = '', courseName = '', courseType = '', courseSubType = '', credit = '';
  var postUrl = '';
  var domain = '';
  var skipSalesForce = '';

  function successAddLearningHandler(res){
    console.log('Saved on SF Service.', res);
  }
  function errorAddLearningHandler(error, status){
    console.log('Error Occurred on SF Service', error, status);
  }

  function addLearning(scoreParam) {
    var url = postUrl + "?domain=" + domain;
    url+="&skipSalesForce=" + skipSalesForce;
    if(scoreParam != undefined){
      url += scoreParam;
    }
    console.log("Post URL: " + url);
    console.log(postData["progress"] != undefined ? postData["progress"]:'');
    var request = $http({
      method: "post",
      url: url,
      withCredentials: "true",
      headers: {
        'Content-Type': 'application/json; charset=utf-8'
      },

      data: JSON.stringify(postData)
    });
    return (request.then(handleSuccess, handleError));
  }


  //////// Interface call from controllers
  function sfInterface(_data, scoreParam){
    postData = {
      "courseID": courseID,
      "name": courseName,
      "type": courseType,
      "subType": courseSubType,
      'credit' : credit,
      "status": _data.status
    };
    if(_data.startDate != undefined){
      postData['startDate'] = _data.startDate;
    }
    else{
      //temporary because there is a bug in the service that requires start date
      var d = new Date();
      var currentDate = d.getFullYear() + "-" + pad(d.getMonth()+1) + "-" +
        pad(d.getDate()) + "T" + pad(d.getHours()) + ":" +
        pad(d.getMinutes()) + ":" + pad(d.getSeconds()) + "Z";

      postData['startDate'] = currentDate;
    }
    if(_data.progress != undefined){
      postData["progress"] =  _data.progress;
    }
    if (_data.status == "COMPLETED_PASSED"){
      postData['completionDate'] = _data.completionDate;
    }
    addLearning(scoreParam).then(successAddLearningHandler, errorAddLearningHandler);
  }
  function  pad(n){return n<10 ? '0'+n : n};

  function handleError(response) {
    //console.log('Handle Error.')
    //console.log(response);
    if (!angular.isObject(response.data) ||
      !response.data.message
    ) {
      return ($q.reject("An unknown error occurred."));
    }
    // Otherwise, use expected error message.
    return ($q.reject(response));
  }
  function handleSuccess(response) {
    if(response.data == "")
      return "";
    else
      return angular.fromJson(response.data);
  }

  /**
   *@desc Expose the public methods. that will be used to hit the server.
   **/
  return ({
    sfInterface :sfInterface,
    setCourseID: function(courseId){
      courseID = courseId;
    },
    setCourseName: function(name){
      courseName = name;
    },
    setType: function(type){
      courseType = type
    },
    setSubType: function(subType){
      courseSubType = subType;
    },
    setCredit: function(_credit){
      credit = _credit;
    },
    setPostUrl:function(url){
      postUrl = url;
    },
    setDomain: function(_domain){
      domain = _domain;
    },
    setSkipSalesForce: function(_skipSalesForce) {
      skipSalesForce = _skipSalesForce;
    }
  });
}]);

angular.module('courseApp', ['SFService'])
  .controller('CourseController', ['$scope', '$http', '$timeout', 'SFService', function($scope, $http, $timeout, SFService){
    function  pad(n){return n<10 ? '0'+n : n};

    function getDateTime(){
      var d = new Date();
      var currentDate = d.getFullYear() + "-" + pad(d.getMonth()+1) + "-" +
        pad(d.getDate()) + "T" + pad(d.getHours()) + ":" +
        pad(d.getMinutes()) + ":" + pad(d.getSeconds()) + "Z";
      return currentDate;
    }

    var invokeSFServiceForStatus = function(status, score){
      var param = {
        "startDate": getDateTime(),
        "status": status
      };

      var scoreParam = "";
      if(status == "COMPLETED_PASSED") {
        score = score != undefined ? score: 100;
        scoreParam = "&score=" + score;
      }

      SFService.sfInterface(param, scoreParam);
    };

    window.angularScope = $scope;
  
    $scope.player_status = 'pause';
    $scope.sound_status = 'volume-up';
    $scope.show_save_exit_modal = false;
    $scope.module_name = '';
    $scope.time = '0:00';

    window._playbar.addListener(PlaybarEvents.CHANGE, function (event) {
      if ($scope.player_status === 'play') {
        window.cp.playPause(!0);

        $scope.player_status = 'pause';
      }

      cp.currentWindow.cpAPIInterface.navigateToTime(event.properties.current * 1000);
    });

    $scope.closeSaveAndExit = function() {
      $scope.show_save_exit_modal = false;
    };;

    $scope.togglePlayPause = function() {
      window.cp.playPause(!0);
      $scope.player_status = $scope.player_status === 'pause' ? 'play' : 'pause';
    };

    $scope.toggleSound = function() {
      cp.toggleMute();
      $scope.sound_status = $scope.sound_status === 'volume-up' ? 'volume-off' : 'volume-up';
    };

    $scope.playNextSlide = function() {;
      cp.goToNextSlide();
      $scope.player_status = 'pause';
    };

    $scope.playPreviousSlide = function () {
      cp.goToPreviousSlide();
      $scope.player_status = 'pause';
    };

    $scope.closeExam = function() {
      DoCPExit && DoCPExit();
    };;

    $scope.helpClickHandler = function() {
      $(".help .popover").toggleClass("block");
      $(".help > a ").toggleClass("menu-active");
    };

    $scope.saveAndExitHandler = function () {
      $scope.show_save_exit_modal = true;
      window.cp.movie.pause(window.cp.ReasonForPause.PLAYBAR_ACTION);
      $scope.player_status = 'play';
    }

    var lastSeconds = -1;

    setInterval(function () {
      if (window.cp) {
        if (!_playbar.chapters || !_playbar.chapters.length) {
          _playbar.chapters = [];
          cp.movie.stage.slides.forEach(function (slideName, ix) {
            if (cp.model.data[slideName].from !== 1) {
              _playbar.chapters.push({
                position: cp.model.data[slideName].from / 30,
                text: 'Slide ' + ix + ': ' + framesToMMSS(cp.model.data[slideName].from)
              });
            }
          });
        }

        var secondsCount = cp.getCpElapsedMovieTime() / 1000,
            minutes = Math.floor(secondsCount / 60),
            seconds = Math.floor(secondsCount % 60),
            totalSeconds = cp.D.project_main.to / 30,
            totalMinutes = Math.floor(totalSeconds / 60),
            totalSeconds = Math.floor(totalSeconds % 60);

        _playbar.total = cp.D.project_main.to / 30;
        _playbar.current = cp.getCpElapsedMovieTime() / 1000;

        if (!isNaN(secondsCount))
        {
          $scope.time = minutes + ':' + (seconds < 10 ? '0' : '') + seconds
                        + ' / ' +
                        totalMinutes + ':' + (totalSeconds < 10 ? '0' : '') + totalSeconds;

          // If we pass the 5 second mark, we say in progress.
          if (lastSeconds < 5 && secondsCount > 5) {
            console.log('IN_PROGRESS');
            invokeSFServiceForStatus('IN_PROGRESS');
          }
          // If we pass the 5 second mark before the end of the video, we say fully completed
          else if (lastSeconds < totalSeconds - 5 && secondsCount > totalSeconds - 5) {
            console.log('COMPLETED_PASSED');
            invokeSFServiceForStatus('COMPLETED_PASSED', 100);
          }

          lastSeconds = secondsCount;
        }
      }
    }, 100);
  }]);

function framesToMMSS(frames) {
  var seconds = frames / 30;
  var minutes = Math.floor(seconds / 60);
  var seconds = Math.floor(seconds % 60);

  return minutes + ':' + (seconds < 10 ? '0' : '') + seconds;

}
